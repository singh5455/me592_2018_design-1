import numpy as np
from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD

def _vectorize_data(input_data, apply_pca=False):
    res_list = []
    for run in input_data:
        # For each run we have [outputs, input]
        op_geo = run[0]

        # Output geometry array
        #op_geo_out = []
        for opg in op_geo:
            ctrlpts = opg[0]
            temperature = opg[1]
            pressure = opg[2]
            ctrlpts_out = _ctrlpts_preprocess(ctrlpts)
            # Apply PCA to reduce surface dimension from 612 to 1, so each surface can be represented by a single number
            if apply_pca:
                ctrlpts_out = _pca(ctrlpts_out)
            #op_geo_out.append(np.array([ctrlpts_out[0], ctrlpts_out[1], ctrlpts_out[2], temperature, pressure]))
            res_list.append(np.array([ctrlpts_out[0], ctrlpts_out[1], ctrlpts_out[2], temperature, pressure]))

        #res_list.append(op_geo_out)
    return np.array(res_list)


def _ctrlpts_preprocess(point_list):
    out_list = []
    for pts in point_list:
        # Flatten in column major
        out_list.append(pts.flatten('F'))
    return out_list


def _pca(X, no_dims=1):
    n = len(X)
    X = X - np.tile(np.mean(X, 0), (n, 1))
    (l, M) = np.linalg.eig(np.dot(X.T, X))
    Y = (np.dot(X, M[:, 0:no_dims])).astype(np.float)
    return Y


def tsne(input_data):
    # So, we have a dataset X containing N data points; N = 64 (# of runs) or all final_geos == 1137
    # Each data point has D dimensions; 3 vectorized surfaces, temperature and pressure = 5

    # First, do the vectorization, rearrange the data array and get the output geometries
    output_geos = _vectorize_data(input_data, apply_pca=True)

    # TSNE code here
    X_reduced = TruncatedSVD(n_components=3, random_state=0).fit_transform(output_geos)
    X_embedded = TSNE(n_components=2, perplexity=100, verbose=2).fit_transform(X_reduced)
