import time
import numpy as np


def pca(input_data, measure_time=False):
    tstart = time.time()
    result = _whiten_data(input_data, 'pca')
    if measure_time:
        tstop = time.time()
        print("PCA elapsed time: " + str((tstop - tstart)) + " seconds")
    return result


def zca(input_data, measure_time=False):
    tstart = time.time()
    result = _whiten_data(input_data, 'pca')
    if measure_time:
        tstop = time.time()
        print("ZCA elapsed time: " + str((tstop - tstart)) + " seconds")
    return result


def _whiten_data(input_data, method):
    res_list = []
    for run in input_data:
        # For each run we have [outputs, input]
        op_geo = run[0]
        ip_geo = run[1]

        # Input geometry array has 3 elements, (x, y, z)
        ip_geo_out = _ctrlpts_preprocess(ip_geo, method)

        # Output geometry array
        op_geo_out = []
        for opg in op_geo:
            ctrlpts = opg[0]
            temperature = opg[1]
            pressure = opg[2]
            ctrlpts_out = _ctrlpts_preprocess(ctrlpts, method)
            op_geo_out.append(np.array([ctrlpts_out, temperature, pressure]))

        res_list.append([op_geo_out, ip_geo_out])
    return np.array(res_list)


def _ctrlpts_preprocess(point_list, method='pca'):
    wf = {'pca': _pca_whitening, 'zca': _zca_whitening}
    out_list = []
    for pts in point_list:
        coord_x = pts[:, 0]
        coord_y = pts[:, 1]
        coord_z = pts[:, 2]
        coords_w = np.hstack((wf[method](coord_x), wf[method](coord_y), wf[method](coord_z)))
        out_list.append(coords_w.reshape(204, 3))
    return out_list


def _pca_whitening(x, epsilon=10e-5):
    avg = np.mean(x)
    val = x - np.tile(avg, (np.size(x), 1))
    sigma = np.dot(val, val.T) / x.shape[0]
    U, S, V = np.linalg.svd(sigma)
    pca_white = np.dot(np.dot(np.diag(1.0 / np.sqrt(S + epsilon)), U.T), x)
    return pca_white


def _zca_whitening(x, epsilon=10e-5):
    avg = np.mean(x)
    val = x - np.tile(avg, (np.size(x), 1))
    sigma = np.dot(val, val.T) / x.shape[0]
    U, S, V = np.linalg.svd(sigma)
    zca_white = np.dot(np.dot(np.dot(U, np.diag(1.0 / np.sqrt(S + epsilon))), U.T), x)
    return zca_white
