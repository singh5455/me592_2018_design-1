import os
import numpy as np


def read_input_geometry_set(file_dir):
    file_list = ['smesh.1.1.txt', 'smesh.1.2.txt', 'smesh.1.3.txt']
    content = []
    for file in file_list:
        file_name = file_dir + '/' + file
        try:
            with open(file_name, 'r') as fp:
                fc = fp.readlines()
                fc = [x.strip().split() for x in fc]
                content += fc[5:209]
        except IOError:
            print("Cannot open " + str(file_name) + " for reading")
            return

    # Convert all 3 sets of control points to a NumPy array with data type of float
    all_ctrlptsw_str = np.array(content)
    all_ctrlptsw = all_ctrlptsw_str.astype(np.float)

    # Delete last row (weights)
    all_ctrlpts = np.delete(all_ctrlptsw, 3, 1)

    # Split into 3 NumPy arrays
    ctrlpts_set = np.split(all_ctrlpts, 3)
    #ctrlpts_set = np.reshape(all_ctrlpts, (3, 612))

    # # Reshape into given dimensions
    # ctrlpts_set_rs = []
    # for surf in ctrlpts_set:
    #     ctrlpts_set_rs.append(surf.reshape(17, 12, 3))

    return ctrlpts_set


def read_final_geometry(file_name):
    # The result file contains all 3 geometries
    try:
        with open(file_name, 'r') as fp:
            content = fp.readlines()
            content = [x.strip().split() for x in content]
            # Remove the first row
            content = content[1:len(content)]
    except IOError:
        print("Cannot open " + str(file_name) + " for reading")
        return

    # Convert all 3 sets of control points to a NumPy array with data type of float
    all_ctrlpts_str = np.array(content)
    all_ctrlpts = all_ctrlpts_str.astype(np.float)

    # Split into 3 NumPy arrays
    ctrlpts_set = np.split(all_ctrlpts, 3)
    #ctrlpts_set = np.reshape(all_ctrlpts, (3, 612))

    # # Reshape into given dimensions
    # ctrlpts_set_rs = []
    # for cpts in ctrlpts_set:
    #     ctrlpts_set_rs.append(cpts.reshape(17, 12, 3))

    return ctrlpts_set


def prepare_geometry_data(input_geo_dir, final_geo_dir, time_step):
    temperatures = {'t1': 300, 't2': 350, 't3': 400, 't4': 450, 't5': 500}
    pressures = {'p1': 76, 'p2': 80, 'p3': 84}

    # Operating systems list directories in different order, below line fixes it
    input_geo_dir_list = sorted(os.listdir(input_geo_dir), key=lambda x: int(x.split('run')[1]))

    # Loop through all input geometries and find corresponding final geometries
    result_arr = []
    for input_dir in input_geo_dir_list:
        # Assume that there are only run directories inside input geometry directory
        run_number = input_dir.split('run')

        # Loop through all final geometries and find data for the current run
        final_geo_arr = []
        for final_geo_file in os.listdir(final_geo_dir):
            if final_geo_file.startswith('result_' + str(time_step)) and final_geo_file.endswith(str(run_number[1])):
                # Read file for the geometry data
                final_geo_data = read_final_geometry(final_geo_dir + '/' + final_geo_file)
                # Parse file name to find temperatures and pressures
                file_info = final_geo_file.split('_')
                final_geo = [final_geo_data, temperatures[file_info[2]], pressures[file_info[3]]]
                final_geo_arr.append(np.array(final_geo))

        # Process input geometry for the current run
        input_geo_data = read_input_geometry_set(input_geo_dir + '/' + input_dir)

        geo_comb = [final_geo_arr, input_geo_data]
        result_arr.append(geo_comb)

    # Return results array
    return np.array(result_arr)
