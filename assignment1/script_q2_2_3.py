import numpy as np
from datetime import datetime

# Conditional loading of matplotlib for HPC cluster execution
mpl_loaded = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    mpl_loaded = False

#
# Read CSV file and do data pre-processing
#

# Read from CSV using NumPy
data = np.genfromtxt('energydata_complete.csv',
                     delimiter=',',
                     skip_header=1,
                     dtype=None,  # determine type of the columns from the data itself
                     usecols=(0, -1),  # use the first and the last columns
                     names=['date', 'EnergyConsumption'])

# Convert NumPY bytes array to Python datetime list
date_bytes = data['date']
datelist = []
for db in date_bytes:
    datelist.append(datetime.strptime(db.decode('UTF-8'), '%m/%d/%Y %H:%M'))

# Get energy consumption as a NumPy array
EC = data['EnergyConsumption']

#
# Histogram for all period
#

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.hist(EC, normed=1, facecolor='blue', alpha=0.75, bins=100)
    plt.xlabel('Energy Consumption')
    plt.title('Histogram of Energy Consumption - All Period')
    plt.grid()
    plt.show()

#
# Histogram for the weekly data
#

# Initialize some temporary variables for data processing
econs_list = []  # stores energy consumptions
current_date = datelist[0]
starting_week = datelist[0].isocalendar()[1]
week_delta = 4  # choose any week
econs_week = 0
start_date = None
start_count = False

# Find daily energy consumption for the chosen week
for date, econs in zip(datelist, EC):
    if starting_week + week_delta == date.isocalendar()[1]:
        econs_list.append(econs)

# Looks a little bit ood but Python stores the last value in memory for a while  and we can take advantage of that
# Add the last parameter to the list
#econs_list.append(econs_week)

# Prepare processed data for plotting
Y = np.array(econs_list)

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.hist(Y, normed=1, facecolor='brown', alpha=0.75, bins=50)
    plt.xlabel('Energy Consumption')
    plt.title("Histogram of Energy Consumption (Week " + str(week_delta+1) + ")")
    plt.grid()
    plt.show()
