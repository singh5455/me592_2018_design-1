import sys
import numpy as np
import scipy.stats

f,aa,cl,fsv,ssd,sspl = np.loadtxt('airfoil_self_noise.dat', unpack=True, usecols=[0,1,2,3,4,5])

def getrange(numbers):
    return max(numbers) - min(numbers)

#sys.stdout=open("2.3-Results.txt","w")

print("\tFrequency,\tAngle of Attack,\tChord Length,\tFree-stream Velocity,\tScale Sound Pressure Level\n")
print("Mean=", '\t', "%.4f" % (np.mean(f)),'\t',"%.4f" % (np.mean(aa)),'\t', "%.4f" % (np.mean(fsv)),'\t', "%.4f" % (np.mean(ssd)),'\t', "%.4f" % (np.mean(sspl)),'\n')
print("Standard Deviation=", '\t', "%.4f" % (np.std(f)),'\t',"%.4f" % (np.std(aa)),'\t', "%.4f" % (np.std(fsv)),'\t', "%.4f" % (np.std(ssd)),'\t', "%.4f" % (np.std(sspl)),'\n')
print("Median=",'\t', "%.4f" % (np.median(f)),'\t',"%.4f" %( np.median(aa)),'\t', "%.4f" % (np.median(fsv)),'\t', "%.4f" % (np.median(ssd)),'\t', "%.4f" % (np.median(sspl)),'\n')
print("Kurtosis=",'\t', "%.4f" % (scipy.stats.kurtosis(f)),'\t',"%.4f" % (scipy.stats.kurtosis(aa)),'\t', "%.4f" %(scipy.stats.kurtosis(fsv)),'\t',"%.4f" % (scipy.stats.kurtosis(ssd)),'\t', "%.4f" % (scipy.stats.kurtosis(sspl)),'\n')
print("Skewness=",'\t', "%.4f" % (scipy.stats.skew(f)),'\t', "%.4f" %(scipy.stats.skew(aa)),'\t', "%.4f" %(scipy.stats.skew(fsv)),'\t',"%.4f" %(scipy.stats.skew(ssd)),'\t',"%.4f" %(scipy.stats.skew(sspl)),'\n')
print("Range=", '\t', "%.4f" % (getrange(f)),'\t',"%.4f" % (getrange(aa)),'\t', "%.4f" % (getrange(fsv)),'\t', "%.4f" % (getrange(ssd)),'\t', "%.4f" % (getrange(sspl)),'\n')

#sys.stdout.close()
