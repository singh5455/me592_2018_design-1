import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.dates as mdates
import csv

# Conditional loading of matplotlib for HPC cluster execution
mpl_loaded = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    mpl_loaded = False

## load data file
df = pd.read_csv('energydata_complete.csv', header=[0], parse_dates=[0], squeeze=True)

headers = ['date','Appliances','lights','T1','RH_1','T2','RH_2','T3','RH_3','T4','RH_4','T5','RH_5','T6','RH_6','T7','RH_7','T8','RH_8','T9','RH_9','T_out','Press_mm_hg','RH_out','Windspeed','Visibility','Tdewpoint','Energy Consumption']
## Convert dates to datetime
df['date'] = df['date'].map(lambda x: datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))

## Resample data as days or weeks using Pandas functions
m_resample = df.assign(date=pd.to_datetime(df.date, unit='ms')).resample('D', on='date').mean().reset_index()
s_resample = df.assign(date=pd.to_datetime(df.date, unit='ms')).resample('D', on='date').sum().reset_index()
# wm_resample = df.assign(date=pd.to_datetime(df.date, unit='ms')).resample('W', on='date').mean().reset_index()
# ws_resample = df.assign(date=pd.to_datetime(df.date, unit='ms')).resample('W', on='date').sum().reset_index()

# print(wm_resample['date'],wm_resample['Energy Consumption'],ws_resample['Energy Consumption'])

if mpl_loaded:
    ##Line plots of Energy Consumption for different groups of data
    f, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    ax1.plot(m_resample['date'],m_resample['Energy Consumption'], 'b-',linewidth=0.7)
    ax2.plot(s_resample['date'],s_resample['Energy Consumption'], 'g-',linewidth=0.7)

    # ax1.set_xlim(['2016-01-18 00:00:00','2016-01-24 00:00:00'])
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Mean Energy Consumption', color='b')
    ax2.set_ylabel('Total Energy Consumption', color='g')
    # plt.title('Weekly Energy Consumption')
    # plt.title('Daily Energy Consumption')
    # plt.title('Daily Energy Consumption (1/18/2016 - 1/24/2016)')
    plt.gcf().autofmt_xdate()

    ## Energy Consumption of entire date set
    f1 = plt.figure(1)
    f1, ax1 = plt.subplots()
    ax1.plot(df['date'], df['Energy Consumption'], 'b-',linewidth=0.7)

    # ax1.set_xlim(['2016-01-18 00:00:00','2016-01-24 00:00:00'])
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Energy Consumption')

    plt.gcf().autofmt_xdate()

    ## Histogram of Energy Consumption over entire data set
    f1 = plt.figure(2)
    f1, ax1 = plt.subplots()
    n, bins, patches = ax1.hist(df['Energy Consumption'], 25, facecolor='b')
    plt.grid()
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Energy Consumption')
    plt.gcf().autofmt_xdate()
    plt.show()
